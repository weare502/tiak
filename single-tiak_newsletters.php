<?php
/**
 * 
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TIAK
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header><!-- .entry-header -->

				<nav class="related-pages"></nav>

				<!-- <div class="entry-content">
					<?php the_content(); ?>
				</div> --><!-- .entry-content -->

					<div class="newsletter clear light-grey">
						<div class="container give-padding clear centered">

							<?php
								$newsletter = get_field('newsletter_file');
								$newslink = get_field('newsletter_link');

							if( $newsletter ) : ?>
								<a href="<?= $newsletter['url']; ?>" target="_blank" class="button">Download this Issue</a>
								<p class="centered"><em>Right Click -> Save File/Link As...</em></p>
							<?php elseif( $newslink ) : ?>
								<a href="<?= $newslink; ?>" target="_blank" class="button">View Newsletter</a>
							<?php endif; ?>

						</div>
					</div>

				<footer class="entry-footer">
					<?php // edit_post_link( esc_html__( 'Edit', 'tiak' ), '<span class="edit-link">', '</span>' ); ?>
				</footer><!-- .entry-footer -->
			</article><!-- #post-## -->

		<?php endwhile; // End of the loop. ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>