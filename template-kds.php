<?php
/**
 * Template Name: KDS Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TIAK
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</header><!-- .entry-header -->

				<nav class="related-pages"></nav>

				<div class="entry-content">
					<?php the_content(); ?>
				</div><!-- .entry-content -->

				<div class="kds">

					<div class="container give-padding clear">

						<div class="half">

							<h3>KDS Point Standings</h3>
							
							<?php the_field('kds_point_standings'); ?>

						</div>
							
						<div class="half">
							<h3>Important Documents &amp; Forms</h3>
							<?php the_field('kds_documents'); ?>
						</div>

					</div>

					<div class="light-grey clear">
						
						<p class="centered container give-padding email">Submit all reports via email to
							<a href="mailto:<?php the_field('kds_submission_email'); ?>">
								<?php the_field('kds_submission_email'); ?>			
							</a>
						</p>				

					</div>

					<div class="container give-padding clear">

						<div class="half">

							<h3>Qualifications for Certification</h3>
							
							<?php the_field('kds_qualifications_certification'); ?>

						</div>

						<div class="half">

							<h3>Qualifications for Renewal</h3>
							
							<?php the_field('kds_qualifications_renewal'); ?>

						</div>

					</div>

				</div>

				<footer class="entry-footer">
					<?php edit_post_link( esc_html__( 'Edit', 'tiak' ), '<span class="edit-link">', '</span>' ); ?>
				</footer><!-- .entry-footer -->
			</article><!-- #post-## -->

		<?php endwhile; // End of the loop. ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>