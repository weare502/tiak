<?php
/**
 * 
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TIAK
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<h1 class="entry-title"><?php echo ucwords( get_field('event_type') ) . " - " . get_the_title(); ?></h1>
				</header><!-- .entry-header -->

				<nav class="related-pages"></nav>

				<div class="entry-content">
					<?php // the_content(); ?>
				</div><!-- .entry-content -->

					<div class="event clear">

						<div class="container give-padding clear">
							
							<div class="clear">
								<div class="half cost clear">
									<h4 class="centered">Cost</h4>
									<p class="centered">Members<br><span class="bold">$<?php the_field('event_member_cost'); ?></span>
										<?php if ( ! empty( get_field( 'event_non_member_cost' ) ) ) : ?>
											<br><br>Non-Members<br><span class="bold">$<?php the_field('event_non_member_cost'); ?></span>
										<?php endif; ?>
									</p>
								</div>
								<p class="half time centered">Date: <span class="bold"><?php echo date( 'M j, Y', strtotime( get_field('event_date') ) ); ?></span>
								
									<br><br> Time: <span class="bold"><?php the_field('event_time'); ?></span>

									<?php if ( ! empty( get_field( 'event_location' ) ) ) : ?>
										<br><br> Location: <span class="bold"><?php the_field('event_location'); ?></span>
									<?php endif; ?>
								
								</p>
							</div>
							
							<?php if ( ! post_password_required() ) : ?>
								<p class="centered registration-link"><a href="<?php echo add_query_arg( 'event-name', urlencode( ucwords( get_field('event_type') ) . " - " . get_the_title() ), add_query_arg( 'event', get_the_id(), get_permalink(2197) ) ); ?>" class="button">Register Now</a></p>
							<?php endif; ?>

							<div class="event-information clear">
								<?php if ( ! post_password_required() ) : ?>
									<?php the_field('event_information'); ?>
								<?php else :  ?>
									<?php echo get_the_password_form( get_the_id() ); ?>
								<?php endif; ?>
							</div>

						</div>

					</div>

				<footer class="entry-footer">
					<?php // edit_post_link( esc_html__( 'Edit', 'tiak' ), '<span class="edit-link">', '</span>' ); ?>
				</footer><!-- .entry-footer -->
			</article><!-- #post-## -->

		<?php endwhile; // End of the loop. ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>