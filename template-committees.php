<?php
/**
 * Template Name: Committees Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TIAK
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</header><!-- .entry-header -->

					<nav class="related-pages"></nav>

					<div class="entry-content clear">
						<?php the_content(); ?>
						
						<h2 class="blue-title centered"><span class="border"><span class="bold">Committee</span> Information</span></h2>

						<div class="container committees">
							
							<?php if ( have_rows( 'committees' ) ) : ?>

								<?php while ( have_rows( 'committees' ) ) : the_row(); ?>
									
									<div class="half committee">
										
										<h3 class="committee-title"><?php the_sub_field( 'name' ); ?></h3>

										<div class="description clear"><?php the_sub_field( 'description' ); ?></div>

										<h4 class="blue-title members-title">Committee Chairs</h4>

										<?php if ( have_rows( 'members' ) ) : ?>

											<div class="members">

											<?php while ( have_rows( 'members' ) ) : the_row(); ?>

												<div class="committee-member">
													<?php the_sub_field( 'name' ); ?>
													<?php $email = get_sub_field( 'email' ); ?>
													<?php if ( $email ) : ?>
														<a class="email" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
													<?php endif; ?>
												</div>

											<?php endwhile; ?>

											</div>

										<?php endif; ?>

									</div>

								<?php endwhile; ?>

							<?php endif; ?>

						</div>

					</div><!-- .entry-content -->


					<footer class="entry-footer">
						<?php edit_post_link( esc_html__( 'Edit', 'tiak' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>