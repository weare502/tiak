<?php
/**
 * Template Name: Events Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TIAK
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</header><!-- .entry-header -->

				<nav class="related-pages"></nav>

				<div class="entry-content">
					<?php the_content(); ?>
				</div><!-- .entry-content -->

					<div class="events">

						<div class="ks-tourism-conference centered light-grey clear">

							<div class="give-padding clear">
							
								<h2><span class="bold">Kansas</span> Tourism Conference</h2>

								<div class="clear description">
									<?php the_field('events_conference_description'); ?>
								</div>

								<p class="clear">
									<a href="<?php echo get_permalink(2175); ?>" class="button">Learn More</a>

									<a href="<?php echo get_permalink(2205); ?>" class="button">Buy Tickets</a>
								</p>

							</div>

						</div>

						<div class="upcoming-events" id="events">

							<div class="container give-padding clear">

								<h2 class="centered">Upcoming Events</h2>
									
									<div class="clear">

										<?php 
											$upcoming_events = new WP_Query( array(
												'post_type' => 'tiak_events',
												'posts_per_page' => -1,
												'meta_key' => 'event_date',
												'meta_value' => current_time('Ymd'),
												'meta_compare' => '>=',
												'orderby' => 'meta_value',
												'order' => 'ASC'
											 ) ); 

											foreach ( $upcoming_events->posts as $event ) : ?>
												<div class="upcoming-event center half">

													<a href="<?php echo get_permalink( $event->ID ); ?>" class="inner">
														<h4><?php echo ucwords( get_field( 'event_type', $event->ID ) ) . " - " . $event->post_title; ?></h4>
														<i class="fa fa-calendar"></i><?php echo date( 'M j, Y', strtotime( get_field('event_date', $event->ID ) ) ); ?>
														<br>
														<span class="button">More Info</span>
													</a>

												</div>
											
										<?php endforeach; ?>

									</div>
							</div>
							
						</div>

						<div class="day-on-hill light-grey clear">

							<div class="container give-padding clear">
							
								<h2 class="centered">Day on the Hill</h2>

								<div class="clear description">
									<?php the_field( 'events_day_on_hill_description' ); ?>
								</div>

								<p class="clear centered">
									<a href="<?php echo get_permalink(165); ?>" class="button">Learn More</a>
								</p>

							</div>


						</div>

					</div>

				<footer class="entry-footer">
					<?php // edit_post_link( esc_html__( 'Edit', 'tiak' ), '<span class="edit-link">', '</span>' ); ?>
				</footer><!-- .entry-footer -->
			</article><!-- #post-## -->

		<?php endwhile; // End of the loop. ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>