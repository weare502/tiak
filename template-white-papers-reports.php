<?php
/**
 * Template Name: White Papers & Reports Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TIAK
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</header><!-- .entry-header -->

				<nav class="related-pages"></nav>

				<div class="entry-content">
					<?php the_content(); ?>
				</div><!-- .entry-content -->

				<div class="white-papers-reports">

					<div class="light-grey clear">

						<div class="container give-padding clear tgt-documents">

							<?php if ( have_rows( 'tgt_documents' ) ) : ?>

								<h2 class="centered">TGT Documents</h2>
								
								<?php while ( have_rows( 'tgt_documents' ) ) : the_row(); ?>

									<div class="white-paper">
										<a href="<?php ! empty( get_sub_field('file' ) ) ? the_sub_field('file') : the_sub_field('external_link'); ?>" target="_blank">
											<?php the_sub_field('link_text'); ?>
										</a>
									</div>

								<?php endwhile; ?>

							<?php endif; ?>

						</div>

					</div>

					<div class="container give-padding clear white-papers">

						<?php if ( have_rows( 'white_papers' ) ) : ?>

							<h2 class="centered">White Papers</h2>
							
							<?php while ( have_rows( 'white_papers' ) ) : the_row(); ?>

								<div class="white-paper">
									<a href="<?php the_sub_field('file'); ?>" target="_blank">
										<?php the_sub_field('link_text'); ?>
									</a>
								</div>

							<?php endwhile; ?>

						<?php endif; ?>

					</div>

					<div class="light-grey clear reports">
						
						<div class="container give-padding clear">

							<?php if ( have_rows( 'reports' ) ) : ?>

								<h2 class="centered">Reports</h2>
								
								<?php while ( have_rows( 'reports' ) ) : the_row(); ?>

									<div class="report">
										<a href="<?php the_sub_field('file'); ?>" target="_blank">
											<?php the_sub_field('link_text'); ?>
										</a>
									</div>

								<?php endwhile; ?>

							<?php endif; ?>

						</div>

					</div>

					<?php if ( get_field( 'speakers_bureau_list' ) ) : ?>
					
						<div class="container give-padding clear speakers-bureau-list">

							<h2 class="centered">Speakers Bureau List</h2>

							<div class="centered">
								<p><a class="button" href="<?php the_field('speakers_bureau_list'); ?>" target="_blank" download>
									Download
								</a></p>
								<br>
							</div>

						</div>

					<?php endif; ?>			

				</div>

				<footer class="entry-footer">
					<?php // edit_post_link( esc_html__( 'Edit', 'tiak' ), '<span class="edit-link">', '</span>' ); ?>
				</footer><!-- .entry-footer -->
			</article><!-- #post-## -->

		<?php endwhile; // End of the loop. ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>