<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package TIAK
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="global-cta">
			<a href="<?php echo get_option( 'footer_call_to_action_url', '/' ); ?>"><?php echo get_theme_mod( 'footer_call_to_action', '' ); ?></a>
		</div>
		<div class="contact-info">
			<div class="contact-phone">
				<?php $footer_phone = get_theme_mod( 'phone_number', '' ); ?>
				<p><a href="tel:<?php echo $footer_phone; ?>" class="phone-number"><?php echo $footer_phone ?></a></p>
			</div>
			<div class="contact-address">
				<p><?php echo get_theme_mod( 'address', '' ); ?></p>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script type="text/javascript">var ssaUrl = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'pixel.sitescout.com/iap/88f9cffa0ad8460d';new Image().src = ssaUrl;</script>
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '292146644306199');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=292146644306199&ev=PageView&noscript=1"
/></noscript>
</body>
</html>