<?php
/**
 * The template for displaying the home page.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TIAK
 */

get_header(); ?>

	<div class="container">
		<?php $home_image = get_field('home_hero_image'); ?>
		<?php if ( !empty( $home_image ) ) : ?>
			<img src="<?php echo $home_image['sizes']['large']; ?>" class="home-hero-image" />
		<?php endif; ?>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<?php if ( have_rows('home_call_to_actions' ) ) : ?>
						<header class="entry-header">
							<div class="container give-padding top-cta">
							<?php while ( have_rows( 'home_call_to_actions' ) ) : the_row(); ?>
								<div class="third">
									<a href="<?php the_sub_field('page_link'); ?>">
										<?php the_sub_field('link_text'); ?>
									</a>
								</div>
							<?php endwhile; ?>
							</div>
						</header><!-- .entry-header -->
					<?php endif; ?>

					<div class="entry-content">
						<?php // the_content(); ?>
					</div><!-- .entry-content -->

					<?php $recents = get_posts( array( 
						'posts_per_page' => 3,
						'post_type' => 'post'
					) ); ?>
					
					<div class="clear">

						<div class="recent-posts container give-padding">
			
							<?php global $post; foreach ( $recents as $post ) : setup_postdata( $post ); ?>
								<div class="recent-post third">
									<?php if ( has_post_thumbnail() ) : ?>
										<?php $bg_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' ); ?>
										<div class="featured-image" style="background-image: url(<?php echo $bg_image[0]; ?>);">
											<time><?php the_time('M'); ?><br><span><?php the_time( 'j' ); ?></span></time>
										</div>
									<?php endif; ?>
									<div class="padding">
										<h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php echo $post->post_title; ?></a></h3>
										<?php if ( ! has_post_thumbnail() ) : ?>
											<?php tiak_posted_on(); ?>
											<?php the_excerpt(); ?>
										<?php else : ?>
											<p><a href="<?php the_permalink(); ?>">Read More...</a></p>
										<?php endif; ?>
									</div>
								</div>	
							<?php endforeach; wp_reset_postdata(); ?>

						</div>

					</div>
					
					<div class="clear container give-padding">
						<div class="half">
							<?php tiak_testimonials(); ?>
						</div>
						<div class="half">
							
							<h3 class="blue-title centered"><span class="bold">Upcoming</span> Events</h3>
							<div class="clear upcoming-events">

								<?php 
									$upcoming_events = new WP_Query( array(
										'post_type' => 'tiak_events',
										'posts_per_page' => 4,
										'meta_key' => 'event_date',
										'meta_value' => current_time('Ymd'),
										'meta_compare' => '>=',
										'orderby' => 'meta_value',
										'order' => 'ASC'
									 ) ); 

									foreach ( $upcoming_events->posts as $event ) : ?>

										<div class="upcoming-event center half">
										
											<a href="<?php echo get_permalink( $event->ID ); ?>" class="inner light-grey">
												<h4><?php echo ucwords( get_field( 'event_type', $event->ID ) ) . " - " . $event->post_title; ?></h4>
												<i class="fa fa-calendar"></i><?php echo date( 'M j, Y', strtotime( get_field('event_date', $event->ID ) ) ); ?>
												<br>
												<span class="button">More Info</span>
											</a>

										</div>
									
								<?php endforeach; ?>

							</div>

							<p class="centered"><a class="button" href="<?php echo get_permalink(141); ?>#events">View All Events</a></p>

						</div>
					</div>

					<?php if ( have_rows( 'home_bottom_call_to_actions' ) ) : ?>

						<div class="light-grey clear">

						<div class="container give-padding bottom-cta">
						
						<?php while ( have_rows( 'home_bottom_call_to_actions' ) ) : the_row(); ?>
							
							<div class="third centered">
								
								<?php the_sub_field('icon') ?>
								<h5 class="cta-title"><?php the_sub_field('title'); ?></h5>
								<p><?php the_sub_field('short_description'); ?></p>
								<a href="<?php the_sub_field('link'); ?>" class="button">
									<?php the_sub_field('button_text'); ?>
								</a>

							</div>
						<?php endwhile; ?>
						
						</div>

						</div>

					<?php endif; ?>

					<footer class="entry-footer">
						<?php // edit_post_link( esc_html__( 'Edit', 'tiak' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>