<?php
/**
 * Template Name: Day on the Hill Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TIAK
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</header><!-- .entry-header -->

				<nav class="related-pages"></nav>

				<div class="entry-content">
					<?php the_content(); ?>
				</div><!-- .entry-content -->

				<div class="day-on-the-hill">

					<?php if ( get_field( 'doth_in_season' ) ) : ?>

						<div class="container give-padding clear">

							<div class="half">
								
								<div class="date centered light-grey">

									<div>Save the Date</div>
								
									<div class="day"><?php the_field('doth_event_date'); ?></div>

									<br>

									<div><?php the_field('doth_cta'); ?></div>
									
									<div class="centered"><a href="<?php echo get_permalink(883); ?>" class="button">Register Now</a></div>

								</div>

									
								<div class="hotel light-grey">
									
									<h2 class="centered">Hotel Information</h2>

									<div class="clear">
										<?php the_field('doth_hotel'); ?>
									</div>

								</div>

								<?php if ( have_rows( 'doth_maps' ) ) : ?>

									<div class="maps centered">
										
										<h2 class="centered">Maps</h2>

										<div class="clear">

										<?php while ( have_rows( 'doth_maps' ) ) : the_row(); ?>

											<a href="<?php the_sub_field('file'); ?>" class="button" target="_blank"><?php the_sub_field('name'); ?></a>

										<?php endwhile; ?>

										</div>

									</div>

								<?php endif; ?>

							</div>

							<?php if ( have_rows( 'doth_agenda' ) ) : ?>

								<div class="half agenda light-grey">
									
									<h2 class="centered">Agenda</h2>

									<ul>

									<?php while ( have_rows( 'doth_agenda' ) ) : the_row(); ?>

										<li><?php the_sub_field('item'); ?></li>

									<?php endwhile; ?>

									</ul>

								</div>

							<?php endif; ?>

							

						</div>

						<div class="clear sponsorship-levels light-grey">

							<div class="clear container give-padding">

							<?php if ( have_rows( 'doth_sponsorship_levels' ) ) : ?>

								<div class="half">

									<h2 class="centered">Sponsorship Levels</h2>

									<?php while ( have_rows( 'doth_sponsorship_levels' ) ) : the_row(); ?>
										
										<div class="centered bold"><?php the_sub_field('level'); ?></div>
										<div class="sponsorship-description"><?php the_sub_field('description'); ?></div>

									<?php endwhile; ?>

								</div>

							<?php endif; ?>

								<div class="half call-to-action">

									<div class="inner">

										<h2 class="centered"><?php the_field('doth_cta'); ?></h2>
										
										<div class="centered"><a href="<?php echo get_permalink(883); ?>" class="button">Register Now</a></div>

									</div>

									<div class="clear video">					
										<div class="container give-padding">
											<div class="video-container">
												<?php the_field('doth_video'); ?>
											</div>
										</div>
									</div>

								</div>
							
							</div>



						</div>

					<?php else : ?>

						<div class="coming-soon light-grey">
							<div class="container give-padding clear">
								<h1 class="centered blue-title"><span class="bold">Coming</span> Soon</h1>
								<div class="clear">
									<?php the_field('doth_coming_soon'); ?>
								</div>
							</div>
						</div>

					<?php endif; ?>

				</div>

				<footer class="entry-footer">
					<?php // edit_post_link( esc_html__( 'Edit', 'tiak' ), '<span class="edit-link">', '</span>' ); ?>
				</footer><!-- .entry-footer -->
			</article><!-- #post-## -->

		<?php endwhile; // End of the loop. ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>