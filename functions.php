<?php
/**
 * TIAK functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package TIAK
 */

if ( ! function_exists( 'tiak_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function tiak_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on TIAK, use a find and replace
	 * to change 'tiak' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'tiak', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'tiak' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

}
endif; // tiak_setup
add_action( 'after_setup_theme', 'tiak_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function tiak_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'tiak_content_width', 1440 );
}
add_action( 'after_setup_theme', 'tiak_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function tiak_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'tiak' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
// add_action( 'widgets_init', 'tiak_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function tiak_scripts() {
	wp_enqueue_style( 'tiak-style', get_stylesheet_uri(), array(), '20154547' );

	wp_enqueue_script( 'tiak-fastlivefilter', get_template_directory_uri() . '/bower_components/jquery-fastlivefilter/jquery.fastLiveFilter.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'tiak-navigation', get_template_directory_uri() . '/js/navigation.js', array( 'jquery', 'jquery-ui-accordion' ), '20120209', true );

	wp_enqueue_script( 'tiak-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'tiak_scripts' );

/**
 * Implement the Custom Header feature.
 */
// require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
// require get_template_directory() . '/inc/jetpack.php';

remove_theme_mod('footer_call_to_action_url' );

add_editor_style( get_stylesheet_uri() );


add_action( 'init', function(){

	$labels = array(
		'name'               => _x( 'Testimonials', 'post type general name', 'tiak' ),
		'singular_name'      => _x( 'Testimonial', 'post type singular name', 'tiak' ),
		'menu_name'          => _x( 'Testimonials', 'admin menu', 'tiak' ),
		'name_admin_bar'     => _x( 'Testimonial', 'add new on admin bar', 'tiak' ),
		'add_new'            => _x( 'Add New', 'Testimonial', 'tiak' ),
		'add_new_item'       => __( 'Add New Testimonial', 'tiak' ),
		'new_item'           => __( 'New Testimonial', 'tiak' ),
		'edit_item'          => __( 'Edit Testimonial', 'tiak' ),
		'view_item'          => __( 'View Testimonial', 'tiak' ),
		'all_items'          => __( 'All Testimonials', 'tiak' ),
		'search_items'       => __( 'Search Testimonials', 'tiak' ),
		'parent_item_colon'  => __( 'Parent Testimonials:', 'tiak' ),
		'not_found'          => __( 'No testimonials found.', 'tiak' ),
		'not_found_in_trash' => __( 'No testimonials found in Trash.', 'tiak' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'testimonials' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'menu_icon'			 => 'dashicons-testimonial',
		'menu_position'      => 23,
		'hierarchical'       => false,
		'supports'           => array( 'title', 'editor' )
	);

	register_post_type( 'tiak_testimonials', $args );

	$labels = array(
		'name'               => _x( 'Newsletters', 'post type general name', 'tiak' ),
		'singular_name'      => _x( 'Newsletter', 'post type singular name', 'tiak' ),
		'menu_name'          => _x( 'Newsletters', 'admin menu', 'tiak' ),
		'name_admin_bar'     => _x( 'Newsletter', 'add new on admin bar', 'tiak' ),
		'add_new'            => _x( 'Add New', 'Newsletter', 'tiak' ),
		'add_new_item'       => __( 'Add New Newsletter', 'tiak' ),
		'new_item'           => __( 'New Newsletter', 'tiak' ),
		'edit_item'          => __( 'Edit Newsletter', 'tiak' ),
		'view_item'          => __( 'View Newsletter', 'tiak' ),
		'all_items'          => __( 'All Newsletters', 'tiak' ),
		'search_items'       => __( 'Search Newsletters', 'tiak' ),
		'parent_item_colon'  => __( 'Parent Newsletters:', 'tiak' ),
		'not_found'          => __( 'No newsletters found.', 'tiak' ),
		'not_found_in_trash' => __( 'No newsletters found in Trash.', 'tiak' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'newsletters' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'menu_icon'			 => 'dashicons-email-alt',
		'menu_position'      => 22,
		'hierarchical'       => false,
		'supports'           => array( 'title' )
	);

	register_post_type( 'tiak_newsletters', $args );

	$labels = array(
		'name'               => _x( 'Members', 'post type general name', 'tiak' ),
		'singular_name'      => _x( 'Member', 'post type singular name', 'tiak' ),
		'menu_name'          => _x( 'Members', 'admin menu', 'tiak' ),
		'name_admin_bar'     => _x( 'Member', 'add new on admin bar', 'tiak' ),
		'add_new'            => _x( 'Add New', 'Member', 'tiak' ),
		'add_new_item'       => __( 'Add New Member', 'tiak' ),
		'new_item'           => __( 'New Member', 'tiak' ),
		'edit_item'          => __( 'Edit Member', 'tiak' ),
		'view_item'          => __( 'View Member', 'tiak' ),
		'all_items'          => __( 'All Members', 'tiak' ),
		'search_items'       => __( 'Search Members', 'tiak' ),
		'parent_item_colon'  => __( 'Parent Members:', 'tiak' ),
		'not_found'          => __( 'No Members found.', 'tiak' ),
		'not_found_in_trash' => __( 'No Members found in Trash.', 'tiak' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => false,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'members' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'menu_icon'			 => 'dashicons-groups',
		'menu_position'      => 18,
		'hierarchical'       => false,
		'supports'           => array( 'title' )
	);

	register_post_type( 'tiak_members', $args );

	$labels = array(
		'name'               => _x( 'Events', 'post type general name', 'tiak' ),
		'singular_name'      => _x( 'Event', 'post type singular name', 'tiak' ),
		'menu_name'          => _x( 'Events', 'admin menu', 'tiak' ),
		'name_admin_bar'     => _x( 'Event', 'add new on admin bar', 'tiak' ),
		'add_new'            => _x( 'Add New', 'Event', 'tiak' ),
		'add_new_item'       => __( 'Add New Event', 'tiak' ),
		'new_item'           => __( 'New Event', 'tiak' ),
		'edit_item'          => __( 'Edit Event', 'tiak' ),
		'view_item'          => __( 'View Event', 'tiak' ),
		'all_items'          => __( 'All Events', 'tiak' ),
		'search_items'       => __( 'Search Events', 'tiak' ),
		'parent_item_colon'  => __( 'Parent Events:', 'tiak' ),
		'not_found'          => __( 'No Events found.', 'tiak' ),
		'not_found_in_trash' => __( 'No Events found in Trash.', 'tiak' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'event' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'menu_icon'			 => 'dashicons-calendar-alt',
		'menu_position'      => 17,
		'hierarchical'       => false,
		'supports'           => array( 'title' )
	);

	register_post_type( 'tiak_events', $args );

});

// Callback function to insert 'styleselect' into the $buttons array
function tiak_mce_buttons( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	//var_dump($buttons);
	return $buttons;
}
// Register our callback to the appropriate filter
add_filter('mce_buttons_2', 'tiak_mce_buttons');

// Callback function to filter the MCE settings
function tiak_mce_before_init_insert_formats( $init_array ) {  
	// Define the style_formats array
	$style_formats = array(  
		// Each array child is a format with it's own settings
		array(  
			'title' => 'Button',  
			'selector' => 'a',  
			'classes' => 'button',
			'icon'	   => ' fa fa-hand-pointer-o'
		),  
	);  
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array;  
  
}
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'tiak_mce_before_init_insert_formats' ); 


add_filter('gettext','tiak_members_title');

function tiak_members_title( $input ) {

    global $post_type;

    if( is_admin() && 'Enter title here' == $input && 'tiak_members' == $post_type )
        return 'Enter Member\'s Full Name Here';

    return $input;
}



function tiak_testimonials( $number = 1 ){ ?>

	<div class="give-padding">
		<div class="testimonials">
			<h3 class="testimonials-title"><span class="bold">Hear</span> what our members think about TIAK</h3>
			<?php $testimonials = get_posts( array( 'post_type' => 'tiak_testimonials', 'posts_per_page' => $number, 'orderby' => 'rand' ) ); ?>
			<?php foreach ( $testimonials as $testimonial ) : ?>
				<div class="testimonial">
					<?php echo apply_filters( 'the_content', $testimonial->post_content ); ?>
					<p class="testimonial-meta"><?php the_field( 'testimonial_client_name', $testimonial->ID ); ?>
					<span class="sep">|</span>
					<?php the_field( 'testimonial_company_name', $testimonial->ID ); ?></p>
				</div>
			<?php endforeach; ?>
			<a href="<?php echo get_post_type_archive_link( 'tiak_testimonials' ); ?>" class="button">View More</a>
		</div>
	</div>

<?php }

function tiak_newsletter() { ?>
	<?php $current_issue = get_posts( array( 'post_type' => 'tiak_newsletters', 'posts_per_page' => 1 ) ); ?>
	<?php if ( empty( $current_issue ) ) return; ?>
	<div class="current-newsletter give-padding">
		<h3 class="title"><span class="bold">Kansas</span> Journeys</h3>
		<p>Read the latest edition of our newsletter, Kansas Journeys.</p>	
		<a href="<?php echo get_permalink( $current_issue[0]->ID ); ?>" class="button">Latest Issue</a>
		<div><a href="<?php echo get_post_type_archive_link('tiak_newsletters'); ?>">All Newsletters</a></div>
	</div>
<?php }

add_filter( 'gform_field_value_event_registration_member_price', 'tiak_event_registration_member_price' );

function tiak_event_registration_member_price( $value ) {

	$event_id = $_GET['event'];

	if ( ! empty( $event_id ) ){

		$event_id = intval( $event_id );

		$event_price = get_field( 'event_member_cost', $event_id );
		
		if ( ! empty( $event_price ) ){

			$value = intval( $event_price );

		}
	}

	return $value;
}

add_filter( 'gform_field_value_event_registration_non_member_price', 'tiak_event_registration_non_member_price' );

function tiak_event_registration_non_member_price( $value ) {

	$event_id = $_GET['event'];

	if ( ! empty( $event_id ) ){

		$event_id = intval( $event_id );

		$event_price = get_field( 'event_non_member_cost', $event_id );

		if ( ! empty( $event_price ) ){

			$value = intval( $event_price );

		}
	}

	return $value;
}


require get_template_directory() . '/inc/gravity-forms-hooks.php';


/*  Add responsive container to embeds
/* ------------------------------------ */ 
function tiak_embed_html( $html ) {
    return '<div class="video-container">' . $html . '</div>';
}
 
add_filter( 'embed_oembed_html', 'tiak_embed_html', 10, 3 );

add_action( 'init', 'tiak_user_edit_acf', 1 );

function tiak_user_edit_acf(){
	if ( ! current_user_can( 'edit_acf' ) ){
		add_filter('acf/settings/show_admin', '__return_false');
		add_action('admin_menu', function(){
			remove_menu_page( 'wppusher' );
		});
	}
}

function tiak_fixResetPassword ($message, $key, $user_login, $user_data) {

    return preg_replace('#<(https?:\/\/.+?)>#i', '$1', $message);

} add_filter('retrieve_password_message', 'tiak_fixResetPassword', 100, 4);