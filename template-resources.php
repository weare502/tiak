<?php
/**
 * Template Name: Resources Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TIAK
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</header><!-- .entry-header -->

					<nav class="related-pages"></nav>

					<div class="entry-content">
						<?php the_content(); ?>

						<?php if ( have_rows( 'links' ) ) : ?>

							<div class="clear resource-links">
							
							<?php while ( have_rows( 'links' ) ) : the_row(); ?>

								<div class="resource-link centered clear">
									
									<?php the_sub_field('icon'); ?>

									<h3><?php the_sub_field('title'); ?></h3>

									<p><?php the_sub_field('short_description'); ?></p>

									<a href="<?php the_sub_field('link'); ?>" class="button">View</a>

								</div>

							<?php endwhile; ?>

							</div>

						<?php endif; ?>	

					</div><!-- .entry-content -->

					<footer class="entry-footer">
						<?php edit_post_link( esc_html__( 'Edit', 'tiak' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>