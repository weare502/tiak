/* jshint devel:true */
jQuery( document ).ready( function( $ ){

	var links = $('.current-menu-parent');

	if ( links.length === 0 ){
		links = $('.current-menu-item ');
	}

	links = links.clone();

	var buttons = [];

	links.find('a').each(function(){

		var copy = this;

		$(copy).addClass('button');

		if ( $(copy).attr('href') === window.location.href ){
			$(copy).addClass('current');
		}
		
		buttons.push(copy);

	});

	if ( buttons.length > 1 ){
		// only show the buttons if there are child pages
		$('.related-pages').append( buttons );	
	}	

	$('#toggle-menu').on('click', function(){
		// toggle the navigation
		$('#site-navigation').toggleClass('show');
	});

	$('.accordion').accordion({
		collapsible: true,
		heightStyle: 'content',
		animate: 200,
		active: false
	});

	$('.show-search').on('click', function(){
		$('#page > .search').toggleClass('showing');
		$('input.gsc-input').focus();
	});

    $('#search-members').fastLiveFilter('.members-list',{
    	callback: function(total) {
    		if ( total >= 1 ){
    			// $('members-list').addClass('filtering');
    		}
    	}
    });

    $('#search-members').on('keyup', function(){
    	if ( $(this).val() !== '' ){
    		$('.members-list').addClass('filtering');
    	} else {
    		$('.members-list').removeClass('filtering');
    	}
    });

    $(".gform_wrapper .disable input").attr( 'readonly', 'readonly' );

    $(".gform_wrapper .disable input").on('click', function(){
    	$(this).blur();
    });

    function someFieldsEmpty( $object ){

    	var isEmpty = false;

    	$object.each( function(){
    		// add i for index

    		if ( $(this).val().trim() === '' ){
    			isEmpty = true;
    		}

    	} );

    	return isEmpty;

    }

    

   //  $('body').delegate('#gform_submit_button_5', 'click', function(){

   //  	var $members = $('#ginput_quantity_5_6');

   //  	var $nonmembers = $('#ginput_quantity_5_7');

   //  	if ( $members.val() === '' ){
   //  		$members = 0;
   //  	} else { 
   //  		$members = $members.val();
   //  	}

   //  	if ( $nonmembers.val() === '' ){
   //  		$nonmembers = 0;
   //  	} else {
   //  		$nonmembers = $nonmembers.val();
   //  	}

   //  	var totalTickets = parseInt( $members ) + parseInt( $nonmembers );

   //  	var $cells = $('#field_5_8 .gfield_list_cell input');

   //  	// console.log( $cells.length < ( 3 * totalTickets ) );
   //  	// console.log( someFieldsEmpty( $cells ) );

   //  	if ( $cells.length < ( 3 * totalTickets ) ) {
			// alert('You must add contact information for all the attendees you are purchasing tickets for.');    		
			// return false;
   //  	} else if ( someFieldsEmpty( $cells ) ){
   //  		alert('You must add contact information for all the attendees you are purchasing tickets for.');    		
   //  		return false;
   //  	} else {
   //  		$('#gform_5').trigger('submit');
   //  	}

   //  });

    // $('body').delegate('#gform_submit_button_6', 'click', function(){

    //     var $dayTickets = parseInt( $('#ginput_quantity_6_7').val() );

    //     var sponsorLevel = $('#input_6_6').val();

    //     console.log( $dayTickets );

    //     console.log( sponsorLevel );

    //     if ( (sponsorLevel.toLowerCase().indexOf('event sponsor') !== -1) || (sponsorLevel.toLowerCase().indexOf('hill sponsor') !== -1) ){
    //         sponsorLevel = 2;
    //     } else {
    //         sponsorLevel = 0;
    //     }

    //     $dayTickets = $dayTickets + sponsorLevel;

    //     console.log( $dayTickets );

    //     var rows = $('#field_6_10 .gfield_list tbody tr').length;

    //     if ( $dayTickets !== rows ) {
    //         alert('You must add relevant information for all the attendees you are purchasing tickets for.');            
    //         return false;
    //     } 

    //     $('#gform_6').trigger('submit');

    // });

    // $('body').delegate('#ginput_quantity_6_7', 'blur', function(){

    //     var rows = $('#field_6_10 .gfield_list tbody tr').length;

    //     var sponsorLevel = $('#input_6_6').val();

    //     if ( (sponsorLevel.toLowerCase().indexOf('event sponsor') !== -1) || (sponsorLevel.toLowerCase().indexOf('hill sponsor') !== -1) ){
    //         sponsorLevel = 2;
    //     } else {
    //         sponsorLevel = 0;
    //     }

    //     console.log('sponsor level ' + sponsorLevel );

    //     console.log('rows ' + rows);

    //     var clicks = parseInt( $(this).val() );

    //     console.log('clicks ' + clicks);

    //     var i = 0;

    //     if ( rows > clicks ){
    //         var subtract = rows - clicks;
    //         for ( i = 0; i < subtract; i++ ){
    //             $('#field_6_10 .delete_list_item').last().trigger('click');
    //         }
    //     } else if ( rows < clicks ){
    //         var add = clicks - rows;
    //         for ( i = 0; i < add; i++ ){
    //             $('#field_6_10 .add_list_item').first().trigger('click'); 
    //         }
    //     }

    // });

    // $('body').delegate('#input_6_6', 'blur', function(){

    //     var value = $(this).val();

    //     var add = 0;

    //     var i = 0;

    //     if ( (value.toLowerCase().indexOf('event sponsor') !== -1) || (value.toLowerCase().indexOf('hill sponsor') !== -1) ){
    //         if ( $('#field_6_10 .gfield_list tbody tr').length === 1 ){
    //             add = 1;
    //         } else {
    //             add = 2;
    //         }

    //         for ( i = 0; i < add; i++ ){
    //             $('#field_6_10 .add_list_item').first().trigger('click'); 
    //         }

    //     } else {

    //         for ( i = 0; i < 2; i++ ){
    //             if ( $('#field_6_10 .gfield_list tbody tr').length !== 1 ){
    //                 $('#field_6_10 .delete_list_item').last().trigger('click');
    //             }
    //         }

    //     }
        
    // });




} );
