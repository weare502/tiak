<?php
/**
 * Template Name: Board of Directors Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TIAK
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</header><!-- .entry-header -->

					<nav class="related-pages"></nav>

					<div class="entry-content clear">
						<?php the_content(); ?>
					</div><!-- .entry-content -->

					<div class="container give-padding centered all-directors">
						<a href="<?php the_field( 'directors_full_list' ); ?>" class="button" target="_blank">All Board Members</a>
					</div>

					<div class="container give-padding executives">

						<?php if ( have_rows( 'directors_executive_committee' ) ) : ?>
							
							<?php while ( have_rows( 'directors_executive_committee' ) ) : the_row(); ?>

								<div class="third">
								
									<h3 class="title"><span class="bold"><?php the_sub_field( 'position' ); ?></span> <?php the_sub_field( 'name' ); ?></h3>

									<p>
										<?php the_sub_field( 'entity' ); ?><br>
										<?php the_sub_field( 'address' ); ?>
									</p>

									<p>
										<a href="tel:<?php the_sub_field( 'phone' ); ?>"><?php the_sub_field( 'phone' ); ?></a><br>
										FAX: <?php the_sub_field( 'fax' ); ?><br>
										<a href="mailto:<?php the_sub_field( 'email' ); ?>"><?php the_sub_field( 'email' ); ?></a>
									</p>

								</div>

							<?php endwhile; ?>

						<?php endif; ?>

					</div>

					<div class="container give-padding centered meeting-schedule">
						<h4 class="title"><span class="bold">Meeting</span> Schedule</h4>
						<a href="<?php the_field('directors_meeting_schedule'); ?>" target="_blank" class="button">Download Schedule</a>
					</div>

					<footer class="entry-footer">
						<?php edit_post_link( esc_html__( 'Edit', 'tiak' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>