<?php
/**
 * Template Name: Contact Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TIAK
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</header><!-- .entry-header -->

					<nav class="related-pages"></nav>

					<div class="entry-content">
						<?php the_content(); ?>

						<div class="half">
							<h2>Send us a message</h2>
							<?php 
								$form = get_field('contact_form');
								echo do_shortcode( "[gravityform id='{$form['id']}' ajax='true' title='false' description='false']" ); 
							?>
						</div>


						<div class="half">
							<?php 
								$map = get_field('contact_map_address');
								echo do_shortcode("[pw_map address='{$map['address']}']");
							?>
							
							<div class="third centered">
								<h3>Address</h3>
								<p><?php echo get_theme_mod( 'address', '' ); ?></p>
							</div>
							
							<div class="third centered">
								<h3>Phone</h3>
								<?php $footer_phone = get_theme_mod( 'phone_number', '' ); ?>
								<p><a href="tel:<?php echo $footer_phone; ?>" class="phone-number"><?php echo $footer_phone ?></a></p>
							</div>

							<div class="third centered">
								<h3>Email</h3>
								<?php $contact_email = get_field('contact_email_address'); ?>
								<a href="mailto:<?php echo $contact_email; ?>">Click to Email</a>
							</div>

						</div>

					</div><!-- .entry-content -->

					<footer class="entry-footer">
						<?php edit_post_link( esc_html__( 'Edit', 'tiak' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>