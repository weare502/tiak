<?php
/**
 * Template Name: Advocacy Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TIAK
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</header><!-- .entry-header -->

					<nav class="related-pages"></nav>

					<div class="entry-content">
						<?php the_content(); ?>
					</div><!-- .entry-content -->

					<div class="container give-padding clear">
						
						<?php if ( have_rows( 'advocacy_faq' ) ) : ?>

							<div class="accordion clear">

							<?php while ( have_rows( 'advocacy_faq' ) ) : the_row(); ?>
					
								<h3 class="question"><?php the_sub_field('question'); ?></h3>
								<div class="answer">
									<?php the_sub_field('answer'); ?>
								</div>

							<?php endwhile; ?>

							</div>

						<?php endif; ?>

					</div>

					<div class="container give-padding clear">
						
						<?php if ( have_rows('advocacy_important_links') ) : ?>

							<div class="important-links clear">

							<h2 class="centered"><span class="bold">Important</span> Links</h2>
							
							<?php while ( have_rows('advocacy_important_links') ) : the_row(); ?>

								<span class="title"><?php the_sub_field('title'); ?></span>
								<span class="link"><a href="<?php the_sub_field('url'); ?>" target="_blank"><?php the_sub_field('url'); ?></a></span>

							<?php endwhile; ?>

							</div>

						<?php endif; ?>

					</div>

					<footer class="entry-footer">
						<?php edit_post_link( esc_html__( 'Edit', 'tiak' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>