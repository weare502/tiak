<?php
/**
 * Template Name: Jobs & Internships Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TIAK
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</header><!-- .entry-header -->

					<nav class="related-pages"></nav>

					<div class="entry-content">
						<?php the_content(); ?>

						<?php if ( have_rows('jobs') ) : ?>
							
							<div class="half">

							<h2>Jobs</h2>

							<?php while ( have_rows('jobs') ) : the_row(); ?>
								
								<?php 
									$phone = get_sub_field('phone');
									$email = get_sub_field('email');
								?>

								<div class="job posting clear">

									<h3><?php the_sub_field('title'); ?></h3>

									<p><em>Salary:</em> <?php the_sub_field('salary'); ?></p>

									<h4>Contact Information</h4>

									<p><a href="tel:"><?php echo $phone; ?></a></p>
									<p><a href="mailto:"><?php echo $email; ?></a></p>

									<h4>Job Description</h4>
									<?php the_sub_field('description'); ?>

								</div>


							<?php endwhile; ?>

							</div>

						<?php else : ?>

							<div class="half">
							<h2>Jobs</h2>
								<p>There are no jobs at this time. Check back later!</p>
							</div>

						<?php endif; ?>
						
						<?php if ( have_rows('internships') ) : ?>
							
							<div class="half">

							<h2>Internships</h2>

							<?php while ( have_rows('internships') ) : the_row(); ?>
								
								<?php 
									$phone = get_sub_field('phone');
									$email = get_sub_field('email');
								?>

								<div class="internship posting clear">

									<h3><?php the_sub_field('title'); ?></h3>

									<p><em>Salary: <?php the_sub_field('salary'); ?></em></p>

									<h4>Contact Information</h4>

									<p><a href="tel:"><?php echo $phone; ?></a></p>
									<p><a href="mailto:"><?php echo $email; ?></a></p>

									<h4>Internship Description</h4>
									<?php the_sub_field('description'); ?>

								</div>


							<?php endwhile; ?>

							</div>

						<?php else : ?>

							<div class="half">
							<h2>Internships</h2>
								<p>There are no internships at this time. Check back later!</p>
							</div>

						<?php endif; ?>

					</div><!-- .entry-content -->

					<footer class="entry-footer">
						<?php edit_post_link( esc_html__( 'Edit', 'tiak' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>