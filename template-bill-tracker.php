<?php
/**
 * Template Name: Bill Tracker Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TIAK
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</header><!-- .entry-header -->

				<nav class="related-pages"></nav>

				<div class="entry-content">
					<?php the_content(); ?>
				</div><!-- .entry-content -->

				<div class="bill-trackers">

					<div class="container give-padding clear">

						<?php if ( have_rows( 'bill_trackers' ) ) : ?>
							
							<?php while ( have_rows( 'bill_trackers' ) ) : the_row(); ?>

								<div class="bill-tracker">
									<a href="<?php the_sub_field('document'); ?>" target="_blank" class="light-grey">
										<?php the_sub_field('title'); ?>
									</a>
								</div>

							<?php endwhile; ?>

						<?php endif; ?>

					</div>

				</div>

				<footer class="entry-footer">
					<?php // edit_post_link( esc_html__( 'Edit', 'tiak' ), '<span class="edit-link">', '</span>' ); ?>
				</footer><!-- .entry-footer -->
			</article><!-- #post-## -->

		<?php endwhile; // End of the loop. ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>