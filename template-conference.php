<?php
/**
 * Template Name: KS Tourism Conference Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TIAK
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</header><!-- .entry-header -->

				<nav class="related-pages"></nav>

				<div class="entry-content">
					<?php the_content(); ?>
				</div><!-- .entry-content -->

					<div class="ks-tourism-conference">

							<?php if ( get_field( 'ktc_in_season' ) ) : ?>

							<div class="container give-padding clear">

								<div class="half">

									<div class="dates light-grey centered clear panel">
										<h2>Conference Dates</h2>
										<p><?php the_field('ktc_start_date'); ?> - <?php the_field('ktc_end_date'); ?></p>

										<p class="centered">
											<a href="<?php echo get_permalink(2205); ?>" class="button">Register Now</a>
										</p>
									</div>

									
									<?php if ( have_rows( 'ktc_agenda' ) ) : ?>

										<div class="agenda light-grey panel">

											<h2 class="centered">Agenda</h2>

											<?php while ( have_rows( 'ktc_agenda' ) ) : the_row(); ?>

												<div class="bold heading"><?php the_sub_field('heading'); ?></div>

												<?php if ( have_rows( 'items' ) ) : ?>

													<ul>

													<?php while ( have_rows( 'items' ) ) : the_row(); ?>

														<li><?php the_sub_field('item'); ?></li>

													<?php endwhile; ?>

													</ul>

												<?php endif; ?>

											<?php endwhile; ?>

											</div>

									<?php endif; ?>

									<div class="sponsor-info light-grey panel">

										<h2 class="centered">Sponsor Information</h2>

										<?php the_field('ktc_sponsor_information'); ?>
									</div>

									<div class="marketing-awards clear light-grey panel">

										<h2 class="centered">Marketing Awards</h2>

										<?php the_field('ktc_marketing_awards'); ?>
										
									</div>

									<div class="support-a-student clear light-grey panel">
										<h2 class="centered">Support a Student</h2>
										<?php the_field('ktc_support_a_student'); ?>
									</div>
									
									<?php if ( ! empty(get_field('ktc_local_products') ) ) : ?>
										<div class="local-products clear light-grey panel">
											<h2 class="centered">Celebrate Local Products</h2>
											<?php the_field('ktc_local_products'); ?>
										</div>
									<?php endif; ?>

								</div>

								<div class="half">

									<div class="hotel light-grey panel">
										<h2 class="centered">Hotel Information</h2>
										<?php the_field('ktc_hotel_info'); ?>
									</div>

									<div class="silent-auction clear light-grey panel">
										<h2 class="centered">Annual Silent Auction</h2>
										<?php the_field('ktc_silent_auction'); ?>
									</div>
									
									<?php if ( ! empty(get_field('ktc_additional_events') ) ) : ?>
										<div class="additional-events clear light-grey panel">
											<h2 class="centered">Additional Events</h2>
											<?php the_field('ktc_additional_events'); ?>
										</div>
									<?php endif; ?>

								</div>
								
							</div>

							<div class="register light-grey clear">
								
								<p class="centered">
									<a href="<?php echo get_permalink(2205); ?>" class="button">Register Now</a>
								</p>

							</div>

						<?php else : ?>

							<div class="coming-soon light-grey">
								<div class="container give-padding clear">
									<h1 class="centered blue-title"><span class="bold">Coming</span> Soon</h1>
									<div class="clear">
										<?php the_field('ktc_coming_soon'); ?>
									</div>
								</div>
							</div>

						<?php endif; ?>

					</div>

				<footer class="entry-footer">
					<?php // edit_post_link( esc_html__( 'Edit', 'tiak' ), '<span class="edit-link">', '</span>' ); ?>
				</footer><!-- .entry-footer -->
			</article><!-- #post-## -->

		<?php endwhile; // End of the loop. ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>