<?php
/**
 * The template for displaying Newsletter archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TIAK
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header container give-padding">
				<?php
					// the_archive_title( '<h1 class="page-title">', '</h1>' );
					// the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
					</header><!-- .entry-header -->

						<div class="newsletter clear">
							<div class="container give-padding clear centered">

								<?php
									$newsletter = get_field('newsletter_file');
									$newslink = get_field('newsletter_link');

								if( $newsletter ) : ?>
									<a href="<?= $newsletter['url']; ?>" target="_blank" class="button">Download this Issue</a>
									<p class="centered"><em>Right Click -> Save File/Link As...</em></p>
								<?php elseif( $newslink ) : ?>
									<a href="<?= $newslink; ?>" target="_blank" class="button">View Newsletter</a>
								<?php endif; ?>
							</div>
						</div>
				</article>

			<?php endwhile; ?>
			<?php the_posts_navigation(); ?>

		<?php else : ?>
			<?php get_template_part( 'template-parts/content', 'none' ); ?>
		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>