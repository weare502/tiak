<?php
/**
 * Template Name: Members Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TIAK
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</header><!-- .entry-header -->

				<nav class="related-pages"></nav>

				<div class="entry-content">
					<?php the_content(); ?>
				</div><!-- .entry-content -->

				<?php if ( ! post_password_required() ) : ?>

					<div class="members">

						<p class="centered">
							<?php if ( !empty( $_GET['order'] ) && $_GET['order'] == 'organization' ) : ?>
								<a href="?order=name" class="button">Order by First Name</a>
							<?php else : ?>
								<a href="?order=organization" class="button">Order by Organization</a>
							<?php endif; ?>
							<br><br>
							<input type="text" id="search-members" placeholder="Type to Search Members" />
						</p>

						<div class="container give-padding clear">
							
							<ul class="members-list">
							

							<?php
								$members = new WP_Query( array( 
										'post_type' => 'tiak_members',
										'posts_per_page' => -1,
										'orderby' => ! empty( $_GET[ 'order' ] ) && $_GET[ 'order' ] == 'organization' ? 'meta_value' : 'post_title',
										'order' => 'ASC',
										'meta_key' => 'member_organization_name'
									) );

								foreach ( $members->posts as $member ) : ?>

									<li class="member third">
										<h4 class="member-name"><?php echo $member->post_title; ?></h4>
										
										<p class="member-organization"><?php echo get_post_meta( $member->ID, 'member_organization_name', true ); ?></p>

										<address class="member-address">
											<?php if ( !empty( get_post_meta( $member->ID, 'member_address_line_1', true ) ) ) : ?>
												<?php echo get_post_meta( $member->ID, 'member_address_line_1', true ); ?><br>
											<?php endif; ?>

											<?php if ( !empty( get_post_meta( $member->ID, 'member_address_line_2', true ) ) ) : ?>
												<?php echo get_post_meta( $member->ID, 'member_address_line_2', true ); ?><br>
											<?php endif; ?>

											<?php if ( !empty( get_post_meta( $member->ID, 'member_city', true ) ) ) : ?>
												<?php echo get_post_meta( $member->ID, 'member_city', true ); ?>
											<?php endif; ?>

											<?php if ( !empty( get_post_meta( $member->ID, 'member_state', true ) ) ) : ?>
												<?php echo get_post_meta( $member->ID, 'member_state', true ); ?>,
											<?php endif; ?>

											<?php if ( !empty( get_post_meta( $member->ID, 'member_zip_code', true ) ) ) : ?>
												<?php echo get_post_meta( $member->ID, 'member_zip_code', true ); ?><br>
											<?php endif; ?>

											<?php if ( !empty( get_post_meta( $member->ID, 'member_county', true ) ) ) : ?>
												<?php echo get_post_meta( $member->ID, 'member_county', true ); ?> County
											<?php endif; ?>
										</address>
										
										<p class="member-contact">

											<?php if ( !empty( get_post_meta( $member->ID, 'member_phone', true ) ) ) : ?>
												Phone: <a href="tel:<?php echo get_post_meta( $member->ID, 'member_phone', true ); ?>" class="phone"><?php echo get_post_meta( $member->ID, 'member_phone', true ); ?></a><br>
											<?php endif; ?>

											<?php if ( !empty( get_post_meta( $member->ID, 'member_fax', true ) ) ) : ?>
												Fax: <?php echo get_post_meta( $member->ID, 'member_fax', true ); ?><br>
											<?php endif; ?>

											<?php if ( !empty( get_post_meta( $member->ID, 'member_email', true ) ) ) : ?>
												Email: <a href="mailto:<?php echo get_post_meta( $member->ID, 'member_email', true ); ?>" class="email"><?php echo get_post_meta( $member->ID, 'member_email', true ); ?></a>
											<?php endif; ?>
										
										</p>

									</li>
									
								<?php endforeach; ?>

								</ul>

						</div>

					</div>

				<?php endif; ?>

				<footer class="entry-footer">
					<?php // edit_post_link( esc_html__( 'Edit', 'tiak' ), '<span class="edit-link">', '</span>' ); ?>
				</footer><!-- .entry-footer -->
			</article><!-- #post-## -->

		<?php endwhile; // End of the loop. ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>