<?php
/**
 * TIAK Theme Customizer.
 *
 * @package TIAK
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function tiak_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	$wp_customize->remove_section( 'static_front_page' );
	$wp_customize->remove_section( 'title_tagline' );
	$wp_customize->get_panel( 'nav_menus' )->capability = 'manage_options';

	$wp_customize->add_section( 'site_options', array( 
		'title' => 'Site Options',
		'priority' => 21
	) );

	$wp_customize->add_setting( 'phone_number', array( 
		'transport' => 'postMessage',
		'default' => '',
		'sanitize_callback' => 'esc_html'
	) );

	$wp_customize->add_control( 'phone_number', array(
		'type' => 'text',
		'label' => 'Phone Number',
		'section' => 'site_options'
	) );

	$wp_customize->add_setting( 'address', array( 
		'transport' => 'postMessage',
		'default' => '',
		'sanitize_callback' => 'esc_html'
	) );

	$wp_customize->add_control( 'address', array(
		'type' => 'text',
		'label' => 'Physical Address',
		'section' => 'site_options'
	) );

	$wp_customize->add_setting( 'footer_call_to_action', array( 
		'transport' => 'postMessage',
		'default' => '',
		'sanitize_callback' => 'esc_html'
	) );

	$wp_customize->add_control( 'footer_call_to_action', array(
		'type' => 'text',
		'label' => 'Footer Call to Action',
		'section' => 'site_options'
	) );

	$wp_customize->add_setting( 'footer_call_to_action_url', array( 
		'transport' => 'refresh',
		'default' => '',
		'sanitize_callback' => 'esc_url',
		'type' => 'option'
	) );

	$wp_customize->add_control( 'footer_call_to_action_url', array(
		'type' => 'text',
		'label' => 'Footer Call to Action URL',
		'description' => __( 'The URL where the call to action will link to when clicked. Don\'t forget the "http://" !', 'tiak' ),
		'section' => 'site_options'
	) );

}
add_action( 'customize_register', 'tiak_customize_register', 100 );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function tiak_customize_preview_js() {
	wp_enqueue_script( 'tiak_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'tiak_customize_preview_js' );
