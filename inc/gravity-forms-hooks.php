<?php 

require 'GWRequireListColumns.php';
require 'GWAutoListFieldRows.php';

// add_filter( 'gform_column_input_5_8_3', 'set_column_5_8_3', 10, 5 );
// function set_column_5_8_3( $input_info, $field, $column, $value, $form_id ) {
//     return array( 'type' => 'select', 'choices' => 'Member,Non-Member' );
// }

new GWAutoListFieldRows( array(
	'form_id' => 5,
	'list_field_id' => 8,
	'input_html_id' => '#ginput_quantity_5_6',
) );

new GWAutoListFieldRows( array(
	'form_id' => 5,
	'list_field_id' => 15,
	'input_html_id' => '#ginput_quantity_5_7',
) );

new GWRequireListColumns(5, 8);

new GWRequireListColumns(5, 15);


add_filter('gform_validation_5', 'gform_validate_minimum_quantity');
function gform_validate_minimum_quantity($validation_result) {
    
    $min_qty = 1;
    $min_qty_message = 'You must order a minimum of %1$d ticket. Order %2$d more tickets to complete your order.';
    $min_qty_fields = array(6, 7);
    
    /* no need to edit below this line */
    
    $form = $validation_result['form'];
    $quantity = 0;
    $qty_fields = array();
    
    foreach($form['fields'] as &$field) {
        
        // if $min_qty_fields specified, make sure only applicable quantity fields are totaled
        if(!empty($min_qty_fields) && !in_array($field['id'], $min_qty_fields))
            continue;
        
        if(in_array(RGFormsModel::get_input_type($field), array('singleproduct', 'calculation'))) {

            // check if product field has separate quantity field, if so skip
            if(sizeof(GFCommon::get_product_fields_by_type($form, array("quantity"), $field['id'])) > 0)
                continue;
                
            $quantity += GFCommon::clean_number(rgpost("input_{$field['id']}_3"));
            $qty_fields[] =& $field;
            
        } else if($field['type'] == 'quantity') {
            $quantity += GFCommon::clean_number(rgpost("input_{$field['id']}"));
            $qty_fields[] =& $field;
        }
        
    }
    
    if($quantity >= $min_qty)
        return $validation_result;
    
    for($i = 0; $i < count($qty_fields); $i++) {
        $qty_fields[$i]['failed_validation'] = true;
        $qty_fields[$i]['validation_message'] = sprintf($min_qty_message, $min_qty, $min_qty - $quantity);
    }
    
    $validation_result['is_valid'] = false;
    $validation_result['form'] = $form;
    
    return $validation_result;
}

// End of File